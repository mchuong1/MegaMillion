/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package megamillion;

/**
 *
 * @author Matthew Chuong
 * This program generates tickets with 5 random numbers plus an extra number
 * Also, it will generates as many tickets as the user wants.
 * I have used arrays and scanner data structures to complete this task.
 * 
 * I have used 6 methods in this program(Bubble, Print, RandomTicket, Random2,
 * Check, and Compare). 
 * Bubble sorts the random numbers into ascending order
 * Print simply prints out arrays
 * RandomTicket generates random numbers for tickets
 * Random2 generates a random bonus number
 * Check makes sure numbers do not repeat in a ticket
 * Compare makes sure the program does not make 2 identical tickets
 * 
 * Input should be numbers 1-5, Yes, yes, No, no
 * Outputs will prompt questions and generate tickets with 5 sorted numbers and a bonus number
 * The first 5 numbers will be between 1-56. The sixth number will be between 1-46.
 */

import java.util.*;

public class MegaMillion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner in = new Scanner(System.in);
        
        int a = 0;//group 2
        
        int[] first = new int[5];//tickets
        int[] sec = new int[5];
        int[] thi = new int[5];
        int[] four = new int[5];
        int[] fifth = new int[5];
        
        while(true)//will make sure to ask the user if he wants more tickets
        {
        System.out.println("Welcome to MegaMillions how many tickets would you like to purchase?(1-5)");//Prompt Question
        int n = in.nextInt();
        
            switch (n) { //conditional loop
                case 1: n = 1;
                    System.out.println("Ticket 1");
                    RandomTicket(first);                    
                    Bubble(first);
                    Check(first);
                    Print(first);                    
                    Random2(a);
                    break;
                case 2: n = 2;
                    RandomTicket(first);
                    RandomTicket(sec);
                    Bubble(first);
                    Bubble(sec);
                    Check(first);
                    Check(sec);
                    Compare(first,sec);
                    System.out.println("Ticket 1");                    
                    Print(first);
                    Random2(a);
                    System.out.println("Ticket 2");                    
                    Print(sec);
                    Random2(a);
                    break;
                case 3: n = 3;
                    RandomTicket(first);
                    RandomTicket(sec);
                    RandomTicket(thi);
                    Bubble(first);
                    Bubble(sec);
                    Bubble(thi);
                    Check(first);
                    Check(sec);
                    Check(thi);
                    Compare(first,sec);
                    Compare(sec,thi);
                    System.out.println("Ticket 1");                    
                    Print(first);
                    Random2(a);
                    System.out.println("Ticket 2");                    
                    Print(sec);
                    Random2(a);
                    System.out.println("Ticket 3");
                    Print(thi);
                    Random2(a);
                    break;
                case 4: n = 4;
                    RandomTicket(first);
                    RandomTicket(sec);
                    RandomTicket(thi);
                    RandomTicket(four);
                    Bubble(first);
                    Bubble(sec);
                    Bubble(thi);
                    Bubble(four);
                    Check(first);
                    Check(sec);
                    Check(thi);
                    Check(four);
                    Compare(first,sec);
                    Compare(sec,thi);
                    Compare(thi,four);
                    System.out.println("Ticket 1");                    
                    Print(first);
                    Random2(a);
                    System.out.println("Ticket 2");                    
                    Print(sec);
                    Random2(a);
                    System.out.println("Ticket 3");
                    Print(thi);
                    Random2(a);
                    System.out.println("Ticket 4");
                    Print(four);
                    Random2(a);
                    break;
                case 5: n = 5;
                    RandomTicket(first);
                    RandomTicket(sec);
                    RandomTicket(thi);
                    RandomTicket(four);
                    RandomTicket(fifth);
                    Bubble(first);
                    Bubble(sec);
                    Bubble(thi);
                    Bubble(four);
                    Bubble(fifth);
                    Check(first);
                    Check(sec);
                    Check(thi);
                    Check(four);
                    Check(fifth);
                    Compare(first,sec);
                    Compare(sec,thi);
                    Compare(thi,four);
                    Compare(four,fifth);
                    System.out.println("Ticket 1");                    
                    Print(first);
                    Random2(a);
                    System.out.println("Ticket 2");                    
                    Print(sec);
                    Random2(a);
                    System.out.println("Ticket 3");
                    Print(thi);
                    Random2(a);
                    System.out.println("Ticket 4");
                    Print(four);
                    Random2(a);
                    System.out.println("Ticket 5");
                    Print(fifth);
                    Random2(a);
                    break;
                default: //will tell them to choose a diff number if it is not 1-5
                    System.out.println("Please choose a number 1-5.");
                    continue;
            }
        System.out.println("Would you like to buy more tickets?");//Repeats the cycle
        String x = in.next();
        if(x.equals("No") || x.equals("no"))//ends the cycle
        {
            break;
        }
        }  
    }
    public static void Random2(int bonus)//generates bonus number
    {
        bonus = 56;
        while(bonus > 46 || bonus == 0)
            {
               int rand = (int)(Math.random()*100);
               bonus = rand;
            }
            System.out.println(bonus);
    }
        public static void RandomTicket(int[] a)//generates random numbers for tickets
    {
        
            for(int i = 0; i < 5; i++)//puts numbers inside of array
            {
                int rand = (int)(Math.random()*100);                    
                while(rand > 56 || rand == 0)// makes sure the numbers are less than 56
                {  
                    rand = (int)(Math.random()*100);
                }
                    
                a[i] = rand;
            }
    }
        public static void Print(int[] a)//prints arrays 
    {
        for(int i = 0; i < 5; i++)
            {
               System.out.print(a[i] + " ");
            }        
    }
        public static void Bubble(int[] a)//sorts the tickets numbers into ascending order
    {
        for(int j = 0; j < 5; j++) //sorting method
        {
        for(int i = 0; i < a.length - 1 ; i++)
        {
            int b;//place holder
            
            if(a[i] > a[i + 1])
            {
                 b = a[i +1];
                 a[i+1] = a[i];
                 a[i] = b;
            }
        }
        }        
    }
        public static void Check(int[] a)//makes sure there is not any repeating numbers
        {
            for(int i = 0; i < 5; i++)
            {
                for(int j = 0; j < a.length - 1; j++)
                {
                    while(a[j] == a[j+1])
                    {
                        a[j+1] =(int)(Math.random()*56);
                    }
                }
            }
        }
        public static void Compare(int[] a, int[] b)// makes sure there are no identicle tickets
        {
            Arrays.toString(a);
            Arrays.toString(b);
            
            if(Arrays.equals(a, b))
            {
                RandomTicket(a);
            }
        }
}
